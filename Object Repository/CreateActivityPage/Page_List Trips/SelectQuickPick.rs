<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SelectQuickPick</name>
   <tag></tag>
   <elementGuidId>066006aa-84d0-44d0-bd05-03a91bb50642</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@type='button' and text()= 'Meal']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@type='button' and text()= 'Meal']</value>
   </webElementProperties>
</WebElementEntity>
