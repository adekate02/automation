<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SelectTimeForActivity</name>
   <tag></tag>
   <elementGuidId>28273928-f730-4ef1-933a-97e8c09a7f3c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[(text() = ' 2 AM ' or . = ' 2 AM ')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='itinerary-list']/div/app-trip-itinerary-calendar/div[2]/div/div/div/div[3]/div/div/mwl-calendar-day-view/div/div[2]/div[3]/mwl-calendar-day-view-hour-segment/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cal-hour-segment cal-hour-start</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> 2 AM </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;itinerary-list&quot;)/div[@class=&quot;row no-gutters&quot;]/app-trip-itinerary-calendar[@class=&quot;col-sm-12&quot;]/div[@class=&quot;row no-gutters&quot;]/div[@class=&quot;col-sm-12&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row no-gutters&quot;]/div[@class=&quot;col-sm-6&quot;]/div[@class=&quot;tab-content mt-4 row&quot;]/div[@class=&quot;col-sm-12&quot;]/mwl-calendar-day-view[1]/div[@class=&quot;cal-day-view&quot;]/div[@class=&quot;cal-hour-rows&quot;]/div[@class=&quot;cal-hour&quot;]/mwl-calendar-day-view-hour-segment[1]/div[@class=&quot;cal-hour-segment cal-hour-start&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='itinerary-list']/div/app-trip-itinerary-calendar/div[2]/div/div/div/div[3]/div/div/mwl-calendar-day-view/div/div[2]/div[3]/mwl-calendar-day-view-hour-segment/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy Forward'])[1]/following::div[15]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jun'])[2]/following::div[19]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Map Data'])[1]/preceding::div[169]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Map data ©2019'])[1]/preceding::div[170]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/mwl-calendar-day-view-hour-segment/div</value>
   </webElementXpaths>
</WebElementEntity>
