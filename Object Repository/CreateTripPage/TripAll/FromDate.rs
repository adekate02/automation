<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FromDate</name>
   <tag></tag>
   <elementGuidId>a9881dc8-de1a-48b3-8006-2a3792252b4b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//table[@class='table table-condensed flush']//td[@class='clickable' and text()=' 10 '])[1]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='flush-bottom text-center flush-left nudge-half--right col-md-6']//td[@class='clickable ng-star-inserted'][contains(text(),'15')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//table[@class='table table-condensed flush']//td[@class='clickable' and text()=' 12 '])[1]</value>
   </webElementProperties>
</WebElementEntity>
