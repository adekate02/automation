<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ToDate</name>
   <tag></tag>
   <elementGuidId>cc77d5c0-7352-4409-8894-28e79a13192b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//table[@class='table table-condensed flush']//td[@class='clickable' and text()=' 15 '])[2]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='col-md-6 flush-bottom flush-right nudge-half--left ng-star-inserted']//td[@class='clickable ng-star-inserted'][contains(text(),'17')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>body > app-root > app-trip-create > div > section > form > div > div > div > div > div:nth-child(3) > div > date-range-picker > div > div > div.col-md-12.flush.text-center > div.col-md-6.flush-bottom.flush-right.nudge-half--left.ng-star-inserted > div.col-md-12.flush > calendar > div.col-md-12.flush > table > tbody > tr:nth-child(4) > td:nth-child(3)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//table[@class='table table-condensed flush']//td[@class='clickable' and text()=' 15 '])[2]</value>
   </webElementProperties>
</WebElementEntity>
