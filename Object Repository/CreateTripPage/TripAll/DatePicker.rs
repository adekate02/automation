<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DatePicker</name>
   <tag></tag>
   <elementGuidId>ba03f29c-e32b-4483-a604-b92497c5baea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@class='form-control date-range-picker-wrapper left-border-wrapper dateRangePicker-input ng-untouched ng-pristine ng-valid']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@class='form-control date-range-picker-wrapper left-border-wrapper dateRangePicker-input ng-untouched ng-pristine ng-valid']
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@class='form-control date-range-picker-wrapper left-border-wrapper dateRangePicker-input ng-untouched ng-pristine ng-valid']</value>
   </webElementProperties>
</WebElementEntity>
