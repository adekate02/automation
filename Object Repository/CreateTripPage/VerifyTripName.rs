<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>VerifyTripName</name>
   <tag></tag>
   <elementGuidId>935cca90-c409-48cc-a4a3-b9ba2ea70af5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//h3[@class='text-capitalize cursor-pointer ng-star-inserted'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//h3[@class='text-capitalize cursor-pointer ng-star-inserted'])[1]</value>
   </webElementProperties>
</WebElementEntity>
