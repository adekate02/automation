<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttonMytrip</name>
   <tag></tag>
   <elementGuidId>243e4b25-de3b-47e5-8572-39a6ca73e786</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html[1]/body[@class=&quot;h-100&quot;]/app-root[1]/app-dashboard[1]/div[@class=&quot;d-flex flex-column h-100&quot;]/app-portal-header[1]/div[@class=&quot;header mb-5 pt-3 pb-3&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 text-center text-md-right&quot;]/ul[@class=&quot;main-menu list-unstyled list-inline mb-0&quot;]/li[@class=&quot;list-inline-item&quot;]/a[1]/i[@class=&quot;trippi-icon icon-tripoutline icon-trip&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='wgg'])[1]/following::i[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>trippi-icon icon-tripoutline icon-trip</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;h-100&quot;]/app-root[1]/app-dashboard[1]/div[@class=&quot;d-flex flex-column h-100&quot;]/app-portal-header[1]/div[@class=&quot;header mb-5 pt-3 pb-3&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 text-center text-md-right&quot;]/ul[@class=&quot;main-menu list-unstyled list-inline mb-0&quot;]/li[@class=&quot;list-inline-item&quot;]/a[1]/i[@class=&quot;trippi-icon icon-tripoutline icon-trip&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='wgg'])[1]/following::i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[2]/preceding::i[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Privacy Policy'])[1]/preceding::i[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//i</value>
   </webElementXpaths>
</WebElementEntity>
