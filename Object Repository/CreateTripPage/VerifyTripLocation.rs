<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>VerifyTripLocation</name>
   <tag></tag>
   <elementGuidId>4d32b824-113e-4344-92ac-f6d56429d3a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//p[@class='trippi-icon icon-mapviewico color-gray'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//p[@class='trippi-icon icon-mapviewico color-gray'])[1]</value>
   </webElementProperties>
</WebElementEntity>
